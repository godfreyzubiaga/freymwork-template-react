import ReactSVG from './assets/react.svg';

const App = () => {
  return (
    <>
      <div className="mx-auto flex min-h-screen w-screen flex-col">
        <header className="py-4">
          <h1 className="text-center text-2xl font-bold">Freymwork Starter Kit</h1>
        </header>

        <main className="flex flex-grow flex-col items-center justify-center p-8">
          <section className="hero rounded-lg bg-white p-6 shadow-md">
            <h2 className="mb-4 text-3xl font-bold">Welcome!</h2>
            <p className="text-lg">This is your starting point for a React, Vite, and Tailwind CSS project.</p>
          </section>
          {/* add a rotating react.svg icon here */}
          <section className="mt-8 flex justify-center">
            <img src={ReactSVG} alt="React logo" className="h-24 w-24 animate-spin-slow" />
          </section>
        </main>

        <footer className="w-full bg-gray-800 py-3 text-center text-white">
          <p>&copy; {new Date().getFullYear()} Freymwork</p>
        </footer>
      </div>
    </>
  );
};

export default App;
